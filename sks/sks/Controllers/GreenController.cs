﻿using sks.ActionFilters;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class GreenController : BaseController
    {
        // GET: Green
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Architecture()
        {
            return View();
        }

        public ActionResult Prevention()
        {
            return View();
        }
    }
}