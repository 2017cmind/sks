﻿using AutoMapper;
using sks.ActionFilters;
using sks.ViewModels.ProductCategory;
using sks.Models;
using sks.Models.Cmind;
using sks.Repositories;
using sks.Utility.Cmind;
using sks.ViewModels.Product;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class BusinessController : BaseController
    {

        // GET: Business
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Office()
        {
            return View();
        }

        public ActionResult POS()
        {
            return View();
        }
        
        public ActionResult InfoSecurity()
        {
            return View();
        }

        public ActionResult Monitor()
        {
            return View();
        }

        public ActionResult Access()
        {
            return View();
        }

        public ActionResult GPS()
        {
            return View();
        }
        
        public ActionResult Security()
        {
            return View();
        }

        public ActionResult AISecurity()
        {
            return View();
        }

        public ActionResult WorkLink()
        {
            return View();
        }

        public ActionResult AIHotel()
        {
            return View();
        }

        //public ActionResult Shop(ProductIndexView model, int type = 1, int CurrentPage = 1)
        //{
        //    var query = productRepository.List(type);
        //    model.PageSize = 9;
        //    var data = query.ToPageResult<Product>(model);
        //    model.PageResult = Mapper.Map<PageResult<ProductView>>(data);

        //    var values = productCategoryRepository.Query(true, null);
        //    model.CategoryList = Mapper.Map<List<ProductCategoryView>>(values);
        //    return View(model);
        //}

        //public JsonResult GetProductList(int type = 1, int CurrentPage = 1)
        //{
        //    var result = "";
        //    var data = productRepository.List(type);
        //    int skipnum = CurrentPage > 1 ? (CurrentPage - 1) * 9 : 0;
        //    var query = data.OrderByDescending(p => p.Sort).Skip(skipnum).Take(9);
        //    foreach (var item in query)
        //    {
        //        result += "<div class='col-6 col-sm-4'>" + "<a href='" + item.Link + "'>" +
        //              "<img class='img-fluid' src='https://wwwadmin.sks.com.tw/FileUploads/ProductPhoto/" + item.MainImage + "' alt='" + item.Name + "' />" +
        //              "<h3 class='h6'>" + item.Name + "</h3>" +
        //              "<p class='font-xs text-gray mb-1'>" + item.Brief + "</p>" +
        //              "<p class='h5 text-blue'>NT." + item.Price + "</p></a></div>";
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
    }
}