﻿using sks.ActionFilters;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Controllers
{
    [LayoutActionFilter]
    [ErrorHandleActionFilter]
    public class LocationController : BaseController
    {
        // GET: Location
        public ActionResult Index(BaseView model)
        {
            return View(model);
        }
    }
}