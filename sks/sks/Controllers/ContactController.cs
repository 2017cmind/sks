﻿using AutoMapper;
using sks.ViewModels.Contact;
using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sks.Repositories;
using sks.Models.MailTemplate;
using sks.Utility.Cmind;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using sks.ActionFilters;
using sks.ViewModels.Shared;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class ContactController : BaseController
    {
        private ContactRepository contactRepository = new ContactRepository();

        public ActionResult Index(int productId = 0)
        {
            ContactView model = new ContactView();
            return View(model);
        }

        public ActionResult Success()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ContactView model)
        {
            if (ModelState.IsValid)
            {
                Contact data = Mapper.Map<Contact>(model);
                data.CreateTime = DateTime.Now;
                model.ID = contactRepository.Insert(data);
                sendMail(model.ID);
                return RedirectToAction("Success", "Contact");
            }
            return View(model);
        }
        #region 寄信
        private bool sendMail(int contactId)
        {
            var data = contactRepository.GetById(contactId);
            ContactView model = Mapper.Map<ContactView>(data);
            ContactMailTemplate mail = new ContactMailTemplate(model);
            return MailHelper.SendEmail(mail);
        }
        #endregion
    }
}