﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using sks.ActionFilters;
using sks.Models;
using sks.Models.MailTemplate;
using sks.Repositories;
using sks.Utility.Cmind;
using sks.ViewModels.Event;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class EventController : BaseController
    {
        private EventRepository eventRepository = new EventRepository();
        private EventContactRepository eventContactRepository = new EventContactRepository();

        // GET: Event
        public ActionResult Index(string id)
        {
            var model = new EventView();
            if (string.IsNullOrEmpty(id)|| id =="Index") {
                return RedirectToAction("Index","Home");
            }
            string name = redirectToFriendUrl(id);
            if (!string.IsNullOrEmpty(name))
            {
                return RedirectToAction("Index", new { id = name });
            }

            if (!string.IsNullOrEmpty(id))
            {
                var query = eventRepository.FindIdByName(id);
                if (query != null)
                {
                    model = Mapper.Map<EventView>(query);
                    model.EventInformations = model.EventInformations.OrderBy(p => p.Sort).ToList();
                    model.EventContactChangeFieldNames = model.EventContactChangeFieldNames.OrderBy(p => p.Sort).ToList();
                    var eventCotactCategoryList = model.EventContactChangeFieldNames.Where(p => p.CoumnId == 0).OrderBy(p => p.CoumnId).ToList();
                    model.EventContactCategoryList = EventConatctCategory(eventCotactCategoryList);
                }
            }
            model.isContact = true;
            return View(model);
        }
        public ActionResult Preview(int id)
        {
            var model = new EventView();
            if (id != 0)
            {
                var query = eventRepository.FindBy(id);
                if (query != null)
                {
                    model = Mapper.Map<EventView>(query);
                    model.EventInformations = model.EventInformations.OrderBy(p => p.Sort).ToList();
                    model.EventContactChangeFieldNames = model.EventContactChangeFieldNames.OrderBy(p => p.Sort).ToList();
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(EventView model)
        {
            var eventid = eventRepository.FindIdByName(model.LinkName).ID;
            model.EventId = eventid;
            model.Address = model.City + model.Area + model.Address;
            model.ID = 0;
            EventContact data = Mapper.Map<EventContact>(model);

            foreach (var modelValue in ModelState.Values)
            {
                modelValue.Errors.Clear();
            }
            var query = eventRepository.FindBy(model.EventId);
            if (query != null)
            {
                model = Mapper.Map<EventView>(query);
                model.EventInformations = model.EventInformations.OrderBy(p => p.Sort).ToList();
                model.EventContactChangeFieldNames = model.EventContactChangeFieldNames.OrderBy(p => p.Sort).ToList();
                var eventCotactCategoryList = model.EventContactChangeFieldNames.Where(p => p.CoumnId == 0).OrderBy(p => p.CoumnId).ToList();
                model.EventContactCategoryList = EventConatctCategory(eventCotactCategoryList);
            }
            if (ModelState.IsValid)
            {
                
                DateTime now = DateTime.Now;
                data.UpdateTime = now;
                data.CreateTime = now;
                var contactId = eventContactRepository.Insert(data);
                bool isSend = sendMail(contactId);
                if (isSend)
                {
                    TempData["Message"] = "填寫表單成功";
                    ShowMessage(true, "填寫表單成功");
                }
                else {
                    TempData["Message"] = "信箱有錯誤";
                    ShowMessage(false, "信箱有錯誤");

                }

                return RedirectToAction("Index", new { id = model.LinkName });
            }

            return View(model);
        }

        #region 寄信
        private bool sendMail(int contactId)
        {
            var eventcontact = eventContactRepository.FindBy(contactId);

            var data = eventRepository.FindBy(eventcontact.EventId);

            EventView model = Mapper.Map<EventView>(eventcontact);
            model.ContactEmail = data.ContactEmail;
            model.MetaTitle = data.MetaTitle;
            var list = data.EventContactChangeFieldNames;
            model.EventContactChangeFieldNames = Mapper.Map<List<EventContactChangeFieldNameView>>(data.EventContactChangeFieldNames);
            model.LinkName = data.LinkName;
            EventContactMailTemplate mail = new EventContactMailTemplate(model);
            return MailHelper.SendEmail(mail);
        }
        #endregion

        private string redirectToFriendUrl(string id)
        {
            string name = string.Empty;
            int numId = 0;
            if (int.TryParse(id, out numId)) {

                Event data = eventRepository.FindBy(numId);
                if (!string.IsNullOrEmpty(data.LinkName))
                {
                    name = data.LinkName;
                }
            }
            return name;
        }
        public List<SelectListItem> EventConatctCategory(List<EventContactChangeFieldNameView> values)
        {
            var result = new List<SelectListItem>();
            result.Insert(0, new SelectListItem { Text = "活動類別(非必選)", Value = "" });
            foreach (var v in values)
            {
                result.Add(new SelectListItem()
                {
                    Value = v.EventCoumnText,
                    Text = v.EventCoumnText
                });
            }
            return result;
        }
        /// <summary>
        /// 顯示訊息
        /// 成功時為綠色訊息
        /// 失敗時為紅色訊息
        /// </summary>        
        /// <param name="success">
        /// 是否成功 成功:true 失敗:false
        /// </param>
        /// <param name="message">
        /// 要顯示的訊息，若帶入空字串則預設成[success==true]為"成功"，[success==false]為"失敗"
        /// </param>
        public void ShowMessage(bool success, string message)
        {
            string tempDataKey = success ? "Result" : "Error";

            if (string.IsNullOrEmpty(message))
                message = success ? "Success" : "Failed";

            this.TempData[tempDataKey] = message;
        }
    }
}