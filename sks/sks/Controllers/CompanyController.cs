﻿using AutoMapper;
using sks.ActionFilters;
using sks.Models;
using sks.Repositories;
using sks.ViewModels.FinancialShareholder;
using sks.ViewModels.Shareholder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class CompanyController : BaseController
    {
        private FinancialShareholderRepository financialShareholderRepository = new FinancialShareholderRepository();

        // GET: Company
        public ActionResult Index(ShareholderView model)
        {
            var filequery = financialShareholderRepository.Query(true, "", 0, 0).ToList();
            model.FileList = Mapper.Map<IEnumerable<FinancialShareholderView>>(filequery);
            model.IsMoblie = IsFromMobile();
            return View(model);
        }
    }
}