﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using sks.ActionFilters;
using sks.ViewModels.News;
using sks.ViewModels.ProductCategory;
using sks.Models;
using sks.Repositories;
using sks.Utility.Cmind;
using sks.ViewModels.Home;
using sks.ViewModels.Shared;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class HomeController : BaseController
    {
        private BannerRepository bannerRepository = new BannerRepository();
        private NewsRepository newsRepository = new NewsRepository();


        public ActionResult Index(HomeView model)
        {
            var bannerquery = bannerRepository.Query(true,"", 0).OrderBy(p => p.Sort);
            model.BannerList = Mapper.Map<IEnumerable<BannerView>>(bannerquery);
            var newsquery = newsRepository.List((int)NewsType.News, 0).OrderByDescending(p => p.OnlineTime).Take(3);
            model.NewsList = Mapper.Map<IEnumerable<NewsView>>(newsquery);
            model.IsFromMobile = IsFromMobile();
            return View(model);
        }

    }
}