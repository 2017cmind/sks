﻿using AutoMapper;
using sks.ActionFilters;
using sks.Models;
using sks.Repositories;
using sks.ViewModels.FinancialShareholder;
using sks.ViewModels.News;
using sks.ViewModels.QA;
using sks.ViewModels.Shareholder;
using sks.ViewModels.ShareholderPolicy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class ShareholderController : BaseController
    {
        private NewsRepository newsRepository = new NewsRepository();

        private FinancialShareholderRepository financialShareholderRepository = new FinancialShareholderRepository();

        private ShareholderPolicyRepository shareholderPolicyRepository = new ShareholderPolicyRepository();

        private QARepository qaRepository = new QARepository();

        // GET: Shareholder
        public ActionResult Index(ShareholderView model)
        {
            var filequery = financialShareholderRepository.Query(true, "", 0 , 0).ToList();
            model.FileList = Mapper.Map<IEnumerable<FinancialShareholderView>>(filequery);

            model.FinancialStatementsList = model.FileList.Where(p => p.Type == (int)FinancialType.FinancialStatements).OrderByDescending(p => p.FiscalYear);
            model.IncomeStatementList = model.FileList.Where(p => p.Type == (int)FinancialType.IncomeStatement).OrderByDescending(p => p.FiscalYear);
            model.InvestorConferenceList = model.FileList.Where(p => p.Type == (int)ShareholderType.InvestorConference).OrderByDescending(p => p.FiscalYear);
            model.RulesList = model.FileList.Where(p => p.Type == (int)GovernanceType.Rules).OrderByDescending(p => p.CreateTime);
            model.TrainingList = model.FileList.Where(p => p.Type == (int)GovernanceType.Training).OrderByDescending(p => p.CreateTime);
            model.ShareholderMeetingList = model.FileList.Where(p => p.Type == (int)ShareholderType.ShareholderMeeting).OrderByDescending(p => p.FiscalYear);
            model.AnnualStatementsList = model.FileList.Where(p => p.Type == (int)FinancialType.AnnualStatements).OrderByDescending(p => p.FiscalYear);
            var newsquery = newsRepository.List((int)NewsType.ShareHolderNews, 0).OrderByDescending(p => p.OnlineTime).ToList();
            model.ShareholderNewsList = Mapper.Map<IEnumerable<NewsView>>(newsquery);

            var shareholderquery = shareholderPolicyRepository.GetById(1);
            model.ShareholderPolicy = Mapper.Map<ShareholderPolicyView>(shareholderquery);

            var qaquery = qaRepository.GetById(1);
            model.QA = Mapper.Map<QAView>(qaquery);

            model.IsMoblie = IsFromMobile();
            return View(model);
        }
    }
}