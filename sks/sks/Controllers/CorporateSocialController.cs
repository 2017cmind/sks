﻿using AutoMapper;
using sks.ActionFilters;
using sks.ViewModels.FinancialShareholder;
using sks.ViewModels.News;
using sks.Models;
using sks.Models.Cmind;
using sks.Repositories;
using sks.Utility.Cmind;
using sks.ViewModels.CorporateSocial;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class CorporateSocialController : BaseController
    {
        private NewsRepository newsRepository = new NewsRepository();

        private FinancialShareholderRepository financialShareholderRepository = new FinancialShareholderRepository();

        // GET: CorporateSocial
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WelfareFoundation(CorporateSocialView model, int year = 0)
        {
            var query = newsRepository.List((int)NewsType.Welfare, year);
            var data = query.AsQueryable().ToPageResult<News>(model);
            model.NewsList = Mapper.Map<PageResult<NewsView>>(data);

            var filequery = financialShareholderRepository.Query(true, "" ,(int)CorporateSocialType.Welfare,0).OrderByDescending(p => p.FiscalYear).ToList();
            model.FileList = Mapper.Map<IEnumerable<FinancialShareholderView>>(filequery);
            return View(model);
        }

        public ActionResult ClassRoom(CorporateSocialView model, int year = 0)
        {
            var query = newsRepository.List((int)NewsType.ClassRoom, year);
            var data = query.AsQueryable().ToPageResult<News>(model);
            model.NewsList = Mapper.Map<PageResult<NewsView>>(data);
            return View(model);
        }

        public ActionResult CultureArtfoundation(CorporateSocialView model, int year = 0)
        {
            var query = newsRepository.List((int)NewsType.CultureAndArts, year);
            var data = query.AsQueryable().ToPageResult<News>(model);
            model.NewsList = Mapper.Map<PageResult<NewsView>>(data);

            var filequery = financialShareholderRepository.Query(true, "", (int)CorporateSocialType.CultureAndArts, 0).OrderByDescending(p =>p.FiscalYear).ToList();
            model.FileList = Mapper.Map<IEnumerable<FinancialShareholderView>>(filequery);
            return View(model);
        }

        public JsonResult GetNewsList(int type, int CurrentPage = 1, int year = 0)
        {
            var result = "";
            var data = newsRepository.List(type, year);
            int skipnum = CurrentPage > 1 ? (CurrentPage - 1) * 10 : 0;
            var query = data.OrderByDescending(p => p.OnlineTime).Skip(skipnum).Take(10);
            result += "<ul class='list-unstyled divider-h mb-4'>";
            foreach (var item in query)
            {
                result += "<li class='py-4 px-lg-10 px-xl-11'>" +
                      "<a href='/News/Detail?id=" + item.ID + "'>" +
                      "<time class='mr-4'>" + item.OnlineTime.ToString("yyyy-MM-dd") + "</time>" +
                      "<span class='d-block d-lg-inline'>"+ item.Title + "</span></a></li>";
            }
            if (data.Count() == 0) {
                result += "<li class='py-4 px-lg-10 px-xl-11 text-center'>查無資料</li>";
            }
            result += "</ul>";

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}