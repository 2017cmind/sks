﻿using sks.ActionFilters;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class SmartHomeController : BaseController
    {
        // GET: SmartHome
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Wireless()
        {
            BaseView model = new BaseView();
            model.IsMoblie = IsFromMobile();
            return View(model);
        }

        public ActionResult Care()
        {
            return View();
        }

        public ActionResult Community()
        {
            return View();
        }
    }
}