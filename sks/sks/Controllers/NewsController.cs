﻿using AutoMapper;
using sks.ActionFilters;
using sks.Models;
using sks.Models.Cmind;
using sks.Repositories;
using sks.Utility.Cmind;
using sks.ViewModels.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Controllers
{
    [ErrorHandleActionFilter]
    public class NewsController : BaseController
    {
        private NewsRepository newsRepository = new NewsRepository();

        // GET: News
        public ActionResult Index(NewsIndexView model, int year = 0)
        {
            var query = newsRepository.List((int)NewsType.News, year);
            var data = query.AsQueryable().ToPageResult<News>(model);
            model.PageResult = Mapper.Map<PageResult<NewsView>>(data);
            return View(model);
        }

        public ActionResult Detail(int id = 0)
        {
            NewsView model = new NewsView();
            if (id != 0) {
                var query = newsRepository.GetById(id);
                model = Mapper.Map<NewsView>(query);
            }
            return View(model);
        }
    }
}