"use strict";

//const { debug } = require("node:util");

$(() => {
  $(window).on('resize scroll', function () {
    $('#mobile-menu').collapse('hide')
    $('#searchBar').removeClass('active')
  })
  if (innerWidth < 1024) {
    $('.mobile-list-tab').on('click', function (e) {
      // console.log($(this))
      $('header .navbar-toggler').click()
    })
  }
  $('.hot-reload').on('click', function () {
    setTimeout(() => location.reload(), 100)
  })
  $('.modal-trigger').on('click', function (e) {
    let videoId = 'https://www.youtube.com/embed/' + $(this).data('video')
    $('#sksVideo iframe').attr('src', videoId)
    $('#main-banner').carousel('pause')
  })
  $('#sksVideo').on('hide.bs.modal', function (e) {
    $('#sksVideo iframe').attr('src', $('#sksVideo iframe').attr('src'))
    $('#main-banner').carousel('cycle')
  })
})

$(() => {
  function agent (browser) {
    return navigator.userAgent.indexOf(browser) >= 0
  }
  function getBorwser () {
    let isMS = 'ActiveXObject' in window
    let browser =
      isMS || agent('MSIE')
        ? 'Edge'
        : agent('Firefox') && !isMS
          ? 'Firefox'
          : agent('Chrome') && !isMS
            ? 'Chrome'
            : agent('Safari') && !isMS
              ? 'Safari'
              : agent('Opera') && !isMS
                ? 'Opera'
                : 'other'
    if (['IE', 'Edge'].indexOf(browser) > -2) {
      $('.btn-menu svg').each(function (e) {
        $(this).css('height', 110)
      })
    }
  }
  getBorwser()
  $(window).on('resize', responsiveList)
  $('#showSearchBar, #closeSearchBar').on('click', function () {
    $('#searchBar').toggleClass('active')
  })

  function scrollDetect () {
    let winTop = $(window).scrollTop()
    let isSrolled = winTop < $('.carousel').height()

      $('header')[(isSrolled ? 'remove' : 'add') + 'Class']('scroll-down')
    //  $('.fix-btn')[(winTop <= last ? 'remove' : 'add') + 'Class']('show')
    return winTop
  }
  $(window).on('load', function () {
    scrollDetect()
    if (location.hash) {
      let hashes = location.hash.split('&')

      //$('.js-aside-investor').toggleClass('d-none',  hashes.includes('#independent'))
      //$('.js-aside-governance').toggleClass('d-none',  !hashes.includes('#independent'))

        const shown = Array(hashes.length).fill(false, 0, hashes.length)
        const showTabtimer = setInterval(() => {
            $.each(hashes, function (i, tab) {
                if ($(tab + '-list').tab('show').hasClass('active')) {
                    shown[i] = true
                }
            })

            if (shown.every(isShown => isShown)) {

                clearInterval(showTabtimer)
            }
        }, 100)
      setTimeout(() => {
        $(window).scrollTop(0)
      }, 150)
    }
  })
  $(window).on('scroll', scrollDetect)
  $('.scroll-href').on('click', function (e) {
    e.preventDefault()
    let href = $($(this).attr('href')).offset().top
    $('html, body').animate({ scrollTop: href }, 1000)
  })
  $('#mobile-menu').on('show.bs.collapse hide.bs.collapse', function (e) {
    $('header')[(e.type === 'show' ? 'add' : 'remove') + 'Class']('open')
  })
  $('.mobile-list-tab').on('click', function (e) {
    $($(this).data('tab')).click()
  })

  function responsiveList () {
    $('.list-responsive').each(function () {
      let totalWidth = 0
      $(this)
        .find('li')
        .each(function () {
          totalWidth += $(this).outerWidth()
        })
      totalWidth += 80
      let parentWidth = $(this).parents('.list-outer').width()
      if (totalWidth > parentWidth) {
        $(this).find('ul').width(totalWidth)
        $(this).parent().addClass('active')
        $(this).click()
      } else {
        $(this).find('ul').width('auto')
        $(this).parent().removeClass('active')
      }
    })

    if ($(window).width() < 1020) {
      $('.jslist').addClass('d-none')
    } else {
      $('.jslist').removeClass('d-none')
    }
  }

  responsiveList()
  $('[role="tablist"]').on('click', function () {
    setTimeout(() => responsiveList(), 200)
  })
  $('.list-controller').on('click', function (e) {
    let action = $(this).data('control')
    let srolled = $(this).siblings('.list-responsive').scrollLeft()
    let dis = $(this).siblings('.list-responsive').width() * 0.4
    srolled = action === 'prev' ? (srolled -= dis) : (srolled += dis)

    $(this).siblings('.list-responsive').animate({ scrollLeft: srolled })
  })

  $('.accordion .collapse').on('show.bs.collapse hide.bs.collapse', function (e) {
    $(this).prev()[(e.type === 'show' ? 'add' : 'remove') + 'Class']('active')
  })
  if ($(window).width() >= 1024) {
      $('#mobile-menu .nav-item').on('mouseenter', function () {
          $(".dropdown-menu.show").removeClass('show')
          $(".nav-item.show").removeClass("show")
          $(this).find('.dropdown-menu').addClass("show");
          $(this).addClass("show")
      })
      $('.dropdown-menu').on('mouseleave', function () {
          //$(this).prev().dropdown('hide')
          $(".dropdown-menu.show").removeClass('show')
          $(".nav-item.show").removeClass("show")
          //$(this).find('.dropdown-menu').addClass("show");
          //$(this).addClass("show")
      })
  }
  $('.change-hash').on('click', function (e) {
    location.hash = $(this).attr('href')
  })
  $('.change-hashes').on('click', function (e) {
    let mainTab = $(this).parents('.tab-pane').attr('id').slice(0)
    location.hash = `${mainTab}&${$(this).attr('href').slice(0)}`
  })

  $('.btn-tab').on('click', function (e) {
    e.preventDefault()
    let section = $(this).attr('href')
    let sectionTop = $(this).offset().top
    $(section)
      .removeClass('d-none')
      .siblings('.tab-container')
      .addClass('d-none')

    $('html, body').animate({ scrollTop: sectionTop }, 1000)
  })
  $('.btn-place-intro').on('click', function (e) {
    e.preventDefault()
    let place = $(this).data('place')
    let title = $(this).prev().text()
    let bg = $(this).parents('.bg-cover').css('background-image')
    $('#title-place').text(title)
    let titleTop = $('#intro-section').offset().top - $('header').height()
    $('html, body').animate({ scrollTop: titleTop }, 1000)

    $('#banner-place').css('background-image', bg)
    $('.service-change').each(function (i, e) {
      if ($(this).data(place)) {
        $(this).addClass('show')
      } else {
        $(this).removeClass('show')
      }
    })

    let bgColor = false
    $('.bg-change').each(function (e) {
      if ($(this).data(place)) {
        if (bgColor) {
          $(this).addClass('bg-gray-ee')
        } else {
          $(this).removeClass('bg-gray-ee')
        }
        bgColor = !bgColor
      }
    })
  })

  $('.list-change').on('click', function (e) {
    e.preventDefault()
    $('.list-change').removeClass('active')
    $(this).addClass('active')
    let item = '#' + $(this).attr('href')
    $(item).removeClass('d-none').siblings().addClass('d-none')
  })
$('.js-list-tab-control').toggleClass('d-none', new URLSearchParams(location.search).get('governance') ==='true')
// 手機版
if(new URLSearchParams(location.search).get('governance') ==='false'){
  setTimeout(() => {
    $(window).trigger('resize')
  }, 500);
}


  // $('#mobile-menu').on('click',function(e){

  //   console.log(new URLSearchParams(location.search).get('governance'))
  //   // if($(e.target).data('hover') ==='公司治理') {
  //   // }
  // })

  // function autoTab () {
  //   if (location.search) {
  //     let search = location.search.substring(1)
  //     console.log(search)
  //     let tab = JSON.parse(
  //       '{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}'
  //     )
  //     console.log(tab)
  //     let list = tab.list
  //     let nav = tab.nav

  //     if (list) {
  //       $('#' + list).tab('show')
  //     }
  //     if (nav) {
  //       $('#' + nav).tab('show')
  //     }
  //   }
  // }
  // autoTab()

  function iOSversion () {
    let d, v
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
      v = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/)
      d = {
        status: true,
        version: parseInt(v[1], 10),
        info:
          parseInt(v[1], 10) +
          '.' +
          parseInt(v[2], 10) +
          '.' +
          parseInt(v[3] || 0, 10)
      }
    } else {
      d = { status: false, version: false, info: '' }
    }
    return d
  }
  function IosCompatibility () {
    if (iOSversion().status) {
      $('text.st2-39').css({ 'letter-spacing': -6 })
      $('text.st41').css({ 'letter-spacing': -8 })
      $('text.st40').css({ 'letter-spacing': -8 })
      $('.bg-office').css({ 'background-attachment': 'initial' })
    }
  }
  IosCompatibility()

  $('.jsindex-link').on('click', function (e) {
    e.preventDefault()
    if ($(window).width() >= 1020) {
      location = $(this).attr('href')
    } else {
      let list = $(this).data('list')

      $(list).toggleClass('d-none')
      setTimeout(() => { $(this).parents('.dropdown-menu').prev().dropdown('show') }, 10)
    }
  })
})
let wow = new WOW({
  boxClass: 'SKS',
  offset: 0,
  mobile: false,
  live: true
  // animateClass: 'animated',
})
wow.init()
