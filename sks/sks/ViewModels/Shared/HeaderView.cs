﻿using sks.ViewModels.ProductCategory;
using sks.Models.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.ViewModels.Shared
{
    public class HeaderView : PageQuery
    {
        public List<ProductCategoryView> ProductMenus { get; set; }
    }
}