﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sks.Models.Cmind;
using sks.ViewModels.FinancialShareholder;
using sks.ViewModels.News;
using sks.ViewModels.Shared;

namespace sks.ViewModels.CorporateSocial
{
    public class CorporateSocialView : HeaderView
    {
        public CorporateSocialView()
        {
            this.Sorting = "OnlineTime";
            this.IsDescending = true;
        }
        public PageResult<NewsView> NewsList { get; set; }

        public IEnumerable<FinancialShareholderView> FileList { get; set; }

    }
}