﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sks.ViewModels.Shared;
using sks.ViewModels.News;
using sks.ViewModels.FinancialShareholder;
using sks.ViewModels.ShareholderPolicy;
using sks.ViewModels.QA;

namespace sks.ViewModels.Shareholder
{
    public class ShareholderView : HeaderView
    {
        /// <summary>
        /// 檔案
        /// </summary>
        public IEnumerable<FinancialShareholderView> FileList { get; set; }
        /// <summary>
        /// 營業收入
        /// </summary>
        public IEnumerable<FinancialShareholderView> IncomeStatementList { get; set; }
        /// <summary>
        /// 財務報告
        /// </summary>
        public IEnumerable<FinancialShareholderView> FinancialStatementsList { get; set; }
        /// <summary>
        /// 公司內規
        /// </summary>
        public IEnumerable<FinancialShareholderView> RulesList { get; set; }
        /// <summary>
        /// 運作情形
        /// </summary>
        public IEnumerable<FinancialShareholderView> TrainingList { get; set; }
        /// <summary>
        /// 股東會
        /// </summary>
        public IEnumerable<FinancialShareholderView> ShareholderMeetingList { get; set; }
        /// <summary>
        /// 年報
        /// </summary>
        public IEnumerable<FinancialShareholderView> AnnualStatementsList { get; set; }

        /// <summary>
        /// 法說會
        /// </summary>
        public IEnumerable<FinancialShareholderView> InvestorConferenceList { get; set; }

        public bool IsMoblie { get; set; }
        public IEnumerable<NewsView> ShareholderNewsList { get; set; }
        public ShareholderPolicyView ShareholderPolicy { get; set; }
        public QAView QA { get; set; }
    }
}