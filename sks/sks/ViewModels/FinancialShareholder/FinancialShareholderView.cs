﻿using sks.Models;
using sks.Repositories;
using sks.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.ViewModels.FinancialShareholder
{
    public class FinancialShareholderView
    {
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "上傳類型")]
        public int isFile { get; set; }

        [Display(Name = "連結")]
        public string Link { get; set; }

        [Required]
        [Display(Name = "類型")]
        public int Type { get; set; }

        [StringLength(50, ErrorMessage = "{0}名稱長度不能超過{1}字. ")]
        [Display(Name = "檔案")]
        public string FileName { get; set; }

        [Display(Name = "年度")]
        public int FiscalYear { get; set; }

        public List<SelectListItem> YearOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var taiwanYear = DateTime.Now.Year - 1911;
                for (int i = taiwanYear; i >= 105; i--)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = i.ToString(),
                        Text = i.ToString()+"年度年報"
                    });
                }
                return result;
            }
        }

        [Display(Name = "上線狀態")]
        public bool Status { get; set; }

        [Display(Name = "創建日期")]
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }
    }
}