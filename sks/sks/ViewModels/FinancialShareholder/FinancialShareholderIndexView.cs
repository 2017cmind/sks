﻿using sks.Models;
using sks.Models.Cmind;
using sks.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.ViewModels.FinancialShareholder
{
    public class FinancialShareholderIndexView : PageQuery
    {
        public FinancialShareholderIndexView()
        {
            this.Sorting = "UpdateTime";
            this.IsDescending = true;
        }

        public PageResult<FinancialShareholderView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "是否上傳檔案")]
        public bool isFile { get; set; }

        [Display(Name = "連結")]
        public string Link { get; set; }

        [Display(Name = "檔案")]
        public string FileName { get; set; }

        [Display(Name = "年度")]
        public int FiscalYear { get; set; }

        [Display(Name = "上線狀態")]
        public bool ? Status { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public List<SelectListItem> YearOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var taiwanYear = DateTime.Now.Year - 1911;
                result.Insert(0, new SelectListItem { Text = "全部", Value = "0" });
                for (int i = taiwanYear; i >= 105; i--)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = i.ToString(),
                        Text = i.ToString()+"年度年報"
                    });
                }
                return result;
            }
        }
    }
}