﻿using sks.Models;
using sks.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.ViewModels.Product
{
    public class ProductView
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int Type { get; set; }

        ProductCategoryRepository productCategoryRepository = new ProductCategoryRepository();

        public List<SelectListItem> TypeOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = productCategoryRepository.List();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Name
                    });
                }
                return result;
            }
        }

        public string MainImage { get; set; }

        public string Brief { get; set; }

        public int Price { get; set; }

        public string Link { get; set; }

        public bool Status { get; set; }

        public int Sort { get; set; }

        public System.DateTime UpdateTime { get; set; }
    }
}