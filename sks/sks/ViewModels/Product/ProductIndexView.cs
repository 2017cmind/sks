﻿using sks.ViewModels.ProductCategory;
using sks.Models;
using sks.Models.Cmind;
using sks.Repositories;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.ViewModels.Product
{
    public class ProductIndexView : HeaderView
    {
        public ProductIndexView()
        {
            this.Sorting = "Sort";
            this.IsDescending = false;
        }

        public PageResult<ProductView> PageResult { get; set; }

        public List<ProductCategoryView> CategoryList { get; set; }

        public int ID { get; set; }

        public string Name { get; set; }

        public int Type { get; set; }

        public bool Status { get; set; }
    }
}