﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sks.ViewModels.QA
{
    public class QAView
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "問題")]
        public string Question { get; set; }

        [Required]
        [Display(Name = "答案")]
        public string Answer { get; set; }

        [Display(Name = "創建日期")]
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }

    }
}