﻿using sks.Models;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.ViewModels.Event
{
    public class EventView : HeaderView
    {
        public EventView()
        {
            this.AreaList = new List<SelectListItem>();
            this.CityList = new List<SelectListItem>();
        }
        public int ID { get; set; }

        [Display(Name = "自定義活動Url")]
        public string LinkName { get; set; }

        [Display(Name = "活動名稱")]
        public string MetaTitle { get; set; }

        [Display(Name = "字型")]
        public FontFamilyType FontFamily { get; set; }

        [Display(Name = "活動簡介")]
        public string MetaDesc { get; set; }

        [Display(Name = "開始時間")]
        public System.DateTime StartDate { get; set; }

        [Display(Name = "結束時間")]
        public System.DateTime EndDate { get; set; }

        [Display(Name = "聯絡我們標題")]
        public string ContactTitle { get; set; }

        [Display(Name = "聯絡我們客服區塊顏色")]
        public string ContactPhoneBgColor { get; set; }

        [Display(Name = "聯絡我們客服文字內容(電話)")]
        public string ContactPhoneText { get; set; }

        [Display(Name = "聯絡我們Email(接收)")]
        public string ContactEmail { get; set; }

        [Display(Name = "聯絡我們說明")]
        public string ContactDesc { get; set; }

        [Display(Name = "聯絡我們背景")]
        public string ContactBg { get; set; }

        [Display(Name = "聯絡我們按鈕文字與樣式")]
        public string ContactBtnText { get; set; }

        [Display(Name = "聯絡我們按鈕背景顏色")]
        public string ContactBtnBgColor { get; set; }

        [Display(Name = "是否顯示聯絡我們表單")]
        public bool isContactStatus { get; set; }

        
        public bool IsVerification { get; set; }


        public EventView Event { get; set; }
        public List<EventInformationView> EventInformations { get; set; }
        public List<EventContactChangeFieldNameView> EventContactChangeFieldNames { get; set; }

        # region 聯絡我們表單
        public int EventId { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [RegularExpression("[^0-9]", ErrorMessage = "電話只能填寫數字")]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "電子信箱")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "地址")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "備註")]
        public string Remark { get; set; }

        
        public List<SelectListItem> EventContactCategoryList { get; set; }

        public string EventContactCategory { get; set; }

        public List<SelectListItem> AreaList { get; set; }

        public List<SelectListItem> CityList { get; set; }

        [Display(Name = "縣/市")]
        public string City { get; set; }

        [Display(Name = "區")]
        public string Area { get; set; }

        [Display(Name = "是否顯示活動表單")]
        public bool isStatus { get; set; }

        [Display(Name = "是否顯示隱私權勾選項目")]
        public bool isContact { get; set; }

        public string ContactRemark { get; set; }

        public DateTime CreateTime { get; set; }

        //驗證碼
        [Display(Name = "驗證碼")]
        public string Verifycode { get; set; }
        #endregion

    }
    #region Information
    public class EventInformationView
    {
        public int ID { get; set; }
        public int EventId { get; set; }
        [Display(Name = "排列方式")]
        public EventInformationType Arrangement { get; set; }

        [Display(Name = "顯示筆數")]
        public int ShowItem { get; set; }

        [Display(Name = "標題")]
        public string HeadLine { get; set; }

        public List<EventInformationItemView> EventInformationItems { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "區塊高度")]
        public string BgHeight { get; set; }
        [Display(Name = "背景顏色")]
        public string BgColor { get; set; }
    }

    public class EventInformationItemView
    {
        public int ID { get; set; }

        public int EventInformationId { get; set; }

        [Display(Name = "背景類型")]
        public ObjectType BgType { get; set; }

        [Display(Name = "背景")]
        public string Background { get; set; }

        [Display(Name = "文字區塊高度")]
        public string TextContentHeight { get; set; }

        [Display(Name = "背景影片連結")]
        public string BackgroundLink { get; set; }

        [Display(Name = "主圖片類型")]
        public ObjectType MainPicType { get; set; }

        [Display(Name = "主圖片")]
        public string MainPic { get; set; }

        [Display(Name = "影片")]
        public string VideoUrl { get; set; }

        [Display(Name = "Icon圖")]
        public string Icon { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "內容")]
        public string Content { get; set; }

        [Display(Name = "文字區塊顏色")]
        public string TextBgColor { get; set; }

        [Display(Name = "文字區塊位置")]
        public CssPosition TextPosition { get; set; }

        [Display(Name = "按鈕連結1")]
        public string Link1 { get; set; }

        [Display(Name = "按鈕連結1名稱")]
        public string LinkName1 { get; set; }

        [Display(Name = "按鈕1背景顏色")]
        public string BtnBgColor1 { get; set; }

        [Display(Name = "按鈕連結2")]
        public string Link2 { get; set; }

        [Display(Name = "按鈕連結2名稱")]
        public string LinkName2 { get; set; }

        [Display(Name = "按鈕2背景顏色")]
        public string BtnBgColor2 { get; set; }

        [Display(Name = "按鈕位置")]
        public CssPosition BtnPosition { get; set; }

        [Display(Name = "是否另開連結")]
        public bool isTargetBlank { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }
    }
    #endregion

    #region EventContactChangeField
    public class EventContactChangeFieldNameView
    {
        public int ID { get; set; }

        public int EventId { get; set; }

        public int CoumnId { get; set; }

        [Display(Name = "分類名稱")]
        public string EventCoumnText { get; set; }

        [Display(Name = "預設欄位名稱")]
        public string EventDefalutCoumnText { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }
    }
    #endregion
    #region
    public class EventContactView
    {
        public int ID { get; set; }

        public int EventId { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "電子信箱")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "地址")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "備註")]
        public string Remark { get; set; }

        public int EventContactChangeFieldNameId { get; set; }

        public EventView Event { get; set; }

        public EventContactChangeFieldNameView EventContactChangeFieldName { get; set; }

        public bool isContact { get; set; }

        public string ContactRemark { get; set; }
    }
    #endregion

}