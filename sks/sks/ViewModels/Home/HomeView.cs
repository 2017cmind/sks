﻿using sks.ViewModels.News;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sks.ViewModels.Home
{
    public class HomeView : HeaderView
    {
        public IEnumerable<BannerView> BannerList { get; set; }
        public IEnumerable<NewsView> NewsList { get; set; }

        public bool IsFromMobile { get; set; }

    }
    public class BannerView
    {
        public int ID { get; set; }

        [Display(Name = "主標題")]
        public string Title { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "標題文字顏色")]
        public int TextColor { get; set; }

        [Display(Name = "電腦版文字位置往下")]
        public bool isOffset { get; set; }

        [Display(Name = "手機版是否文字位置往下")]
        public bool isPhoneOffset { get; set; }

        [Display(Name = "位置")]
        public int Position { get; set; }

        [Display(Name = "標題是否有logo圖")]
        public bool isLogo { get; set; }

        [Display(Name = "副標題")]
        public string Intro { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }

        [Display(Name = "手機圖片")]
        public string MobileImg { get; set; }


        [Display(Name = "Youtube連結")]
        public string VideoLink { get; set; }

        [Display(Name = "YoutubeID")]
        public string VideoId
        {
            get
            {
                if (!string.IsNullOrEmpty(VideoLink))
                {
                    var url = VideoLink.IndexOf("https://www.youtube.com/watch?v=") != -1 ? "https://www.youtube.com/watch?v=" : "https://youtu.be/";
                    var str = VideoLink.Replace(url, "");
                    var result = str.IndexOf("&") != -1 ? str.Substring(0, str.IndexOf("&")) : str;
                    return result;
                }
                return null;
            }
        }

        [Display(Name = "按鈕連結")]
        public string Link { get; set; }

        [Display(Name = "上線狀態")]
        public bool Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }


        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public int Updater { get; set; }
    }
}