﻿using sks.Models.Cmind;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.ViewModels.News
{
    public class NewsIndexView :HeaderView
    {
        public NewsIndexView()
        {
            this.Sorting = "OnlineTime";
            this.IsDescending = true;
        }

        public PageResult<NewsView> PageResult { get; set; }

        public int ID { get; set; }

        public string Title { get; set; }

        public int FiscalYear { get; set; }

        public int Type { get; set; }

        public System.DateTime OnlineTime { get; set; }

        public bool? Status { get; set; }

        public System.DateTime UpdateTime { get; set; }
    }
}