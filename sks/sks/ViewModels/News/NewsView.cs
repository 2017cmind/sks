﻿using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.ViewModels.News
{
    public class NewsView : HeaderView
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public int Type { get; set; }

        public string MainImage { get; set; }

        public string Content { get; set; }

        public System.DateTime OnlineTime { get; set; }

        public bool Status { get; set; }
    }
}