﻿using sks.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sks.ViewModels.ProductCategory
{
    public class ProductCategoryIndexView : PageQuery
    {
        public ProductCategoryIndexView()
        {
            this.Sorting = "Sort";
            this.IsDescending = false;
        }

        public PageResult<ProductCategoryView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "產品類別名稱")]
        public string Name { get; set; }

        [Display(Name = "上線狀態")]
        public bool? Status { get; set; }
    }
}