﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sks.ViewModels.ProductCategory
{
    public class ProductCategoryView
    {
        public int ID { get; set; }

        [Display(Name = "產品類別名稱")]
        public string Name { get; set; }

        [Display(Name = "上線狀態")]
        public bool Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "創建日期")]
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }

    }
}