﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sks.ViewModels.ShareholderPolicy
{
    public class ShareholderPolicyView
    {
        public int ID { get; set; }

        [Display(Name = "內容")]
        public string Content { get; set; }

        [Display(Name = "創建日期")]
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }
    }
}