﻿using sks.Models;
using sks.Utility.Cmind;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.ViewModels.Contact
{
    public class ContactView : HeaderView
    {
        public ContactView()
        {
            this.AreaList = new List<SelectListItem>();
            this.CityList = new List<SelectListItem>();
        }
        public int ID { get; set; }

        [Display(Name = "商品類別")]
        public int ProductId { get; set; }

        public List<SelectListItem> ProductList
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Products>();
                result.Insert(0, new SelectListItem { Text = "商品請選擇(非必選)", Value = "0" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> SubjectList
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ContactType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = EnumHelper.GetDescription(v),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        [Required]
        [StringLength(50, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        public bool Gender { get; set; }

        [Display(Name = "主旨(下拉選單)")]
        public string Subject { get; set; }

        public List<SelectListItem> AreaList { get; set; }

        public List<SelectListItem> CityList { get; set; }


        [Required]
        [StringLength(20, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "電話")]
        public string Phone { get; set; }

        [Required]
        [StringLength(255, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string Email { get; set; }

        [Display(Name = "縣/市")]
        public string City { get; set; }

        [Display(Name = "區")]
        public string Area { get; set; }

        [StringLength(255, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "地址")]
        public string Address { get; set; }

        public string ContactTime { get; set; }

        public List<SelectListItem> ContactTimeList {
            get {
                var result = new List<SelectListItem>();
                result.Add(new SelectListItem { Text = "方便聯絡時間（選填）", Value = "任何時間" });
                result.Add(new SelectListItem { Text = "08:00", Value = "08:00" });
                result.Add(new SelectListItem { Text = "09:00", Value = "09:00" });
                result.Add(new SelectListItem { Text = "10:00", Value = "10:00" });
                result.Add(new SelectListItem { Text = "11:00", Value = "11:00" });
                result.Add(new SelectListItem { Text = "12:00", Value = "12:00" });
                result.Add(new SelectListItem { Text = "13:00", Value = "13:00" });
                result.Add(new SelectListItem { Text = "14:00", Value = "14:00" });
                result.Add(new SelectListItem { Text = "15:00", Value = "15:00" });
                result.Add(new SelectListItem { Text = "16:00", Value = "16:00" });
                result.Add(new SelectListItem { Text = "17:00", Value = "17:00" });
                result.Add(new SelectListItem { Text = "18:00", Value = "18:00" });
                result.Add(new SelectListItem { Text = "19:00", Value = "19:00" });
                result.Add(new SelectListItem { Text = "20:00", Value = "20:00" });
                return result;
            }
        }

        [Required]
        [StringLength(3000, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "留言")]
        public string Message { get; set; }

        public string Remark { get; set; }

        public System.DateTime CreateTime { get; set; }

        ////驗證碼
        //[Required]
        //[Display(Name = "驗證碼")]
        //public string Verifycode { get; set; }
    }
}