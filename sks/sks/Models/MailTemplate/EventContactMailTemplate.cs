﻿using sks.Utility.Cmind;
using sks.ViewModels.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Models.MailTemplate
{
    public class EventContactMailTemplate : MailBase
    {
        private const string SUBJECT = "[新光保全]活動聯絡我們的表單";

        private const string template = @"<p>請盡速回覆</p>
                                          <p>活動名稱：{1}</p>
                                          <p>活動網址：{10}<p/>
                                          <p>序號：{0}<p/>
                                          <p>填寫資訊如下:</p>    
                                          <p>活動聯絡我們表單類別:{2}</p>
                                          <p>姓名：{3}</p>
                                          <p>電話：{4}</p>
                                          <p>電子郵件：{5}</p>
                                          <p>地址：{6}</p>
                                          <p>意見：{7}</p>
                                          <p>是否接受相關促銷活動：{8}</p>
                                          <p>填寫時間：{9}</p><br/>
                                          <p><a href='https://www.sks.com.tw'>新光保全</a></p>";
        public EventContactMailTemplate(EventView model)
        {
            //正式
            this.Email = model.ContactEmail;

            this.Subject = SUBJECT + "，序號" + model.ID ;
            this.MailBody = getMailBody(model);
        }

        private string getMailBody(EventView model)
        {
            string eventContactChangeFieldName = !string.IsNullOrEmpty(model.EventContactCategory) ? model.EventContactCategory : "無";
            string isContact = model.isContact ? "是" :"否";
            string eventUrl = "https://www.sks.com.tw/Event/" + model.LinkName;
            string mailBody = string.Format(template,
                model.ID,
                model.MetaTitle,
                eventContactChangeFieldName,
                model.Name,
                model.Phone,
                model.Email,
                model.Address,
                model.Remark,
                isContact,
                model.CreateTime.ToString("yyyy.MM.dd HH:mm"),
                eventUrl
                );

            return mailBody;
        }
    }
}