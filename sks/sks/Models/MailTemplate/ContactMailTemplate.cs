﻿using sks.Utility.Cmind;
using sks.ViewModels.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Models.MailTemplate
{
    public class ContactMailTemplate : MailBase
    {
        private const string SUBJECT = "[新光保全]聯絡我們的表單";


        private const string template = @"<p>請盡速回覆</p>
                                          <p>序號:{8}<p/>
                                          <p>填寫資訊如下:</p>                                          
                                          <p>姓名： {0}</p>
                                          <p>產品名稱:{7}</p>
                                          <p>電話： {1}</p>
                                          <p>電子郵件： {2}</p>
                                          <p>地址： {9}{10}{3}</p>
                                          <p>方便聯絡時間： {4}</p>
                                          <p>留言： {5}</p>
                                          <p>填寫時間:{6}</p><br/>
                                          <p><a href='https://www.sks.com.tw'>新光保全</a></p>";


        public ContactMailTemplate(ContactView model)
        {
            //正式
            this.Email = "service@sks.com.tw";
            //測試用
            //this.Email = "yk814016@gmail.com";
            this.Subject = SUBJECT + "，序號" + model.ID + "，主旨:" + model.Subject;
            this.MailBody = getMailBody(model);
        }

        private string getMailBody(ContactView model)
        {
            string productStr= model.ProductId != 0 ? model.ProductList.Where(p => p.Value == model.ProductId.ToString()).FirstOrDefault().Text : "無";
            string mailBody = string.Format(template,
                model.Name,
                model.Phone,
                model.Email,
                model.Address,
                model.ContactTime,
                model.Message,
                model.CreateTime.ToString("yyyy.MM.dd HH:mm"),
                productStr,
                model.ID,
                model.City,
                model.Area
                );

            return mailBody;
        }
    }
}