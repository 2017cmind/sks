﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace sks.Models
{
    /// <summary>
    /// 輪播類別
    /// </summary>
    public enum BannerType
    {
        /// <summary>
        /// 圖片
        /// </summary>
        [Description("圖片")]
        Pic = 1,

        /// <summary>
        /// 影片       
        /// </summary>
        [Description("影片")]
        Video = 2,
    }

    /// <summary>
    /// 管理員類別
    /// </summary>
    public enum AdminType
    {
        /// <summary>
        /// 一般管理員
        /// </summary>
        [Description("一般管理員")]
        Admins = 0,

        /// <summary>
        /// 系統管理員       
        /// </summary>
        [Description("系統管理員")]
        Managers = 1,
    }

    /// <summary>
    /// 標題與文字放置位置
    /// </summary>
    public enum PositionType
    {
        /// <summary>
        /// 置左
        /// </summary>
        [Description("置左")]
        Left = 1,
        /// <summary>
        /// 置右       
        /// </summary>
        [Description("置右")]
        Right = 3,
    }

    /// <summary>
    /// 聯絡我們信件主旨
    /// </summary>
    public enum ContactType
    {
        /// <summary>
        /// 業務需求
        /// </summary>
        [Description("業務需求")]
        Business = 1,
        /// <summary>
        /// 問題反應       
        /// </summary>
        [Description("問題反應")]
        Question = 2,
    }

    /// <summary>
    /// 產品
    /// </summary>
    public enum Products
    {
        /// <summary>
        /// 社區雲端守護
        /// </summary>
        [Description("社區雲端守護")]
        Community = 1,

        /// <summary>
        /// 家庭保全服務       
        /// </summary>
        [Description("家庭保全服務")]
        Wireless = 2,

        /// <summary>
        /// 居家照護陪伴       
        /// </summary>
        [Description("居家照護陪伴")]
        Care = 3,

        /// <summary>
        /// 商辦雲端守護       
        /// </summary>
        [Description("商辦雲端守護")]
        Office = 4,

        /// <summary>
        /// SKPOS       
        /// </summary>
        [Description("SKPOS")]
        POS = 5,

        ///<summary>
        /// 資安閘門服務
        /// </summary>
        [Description("資安閘門服務")]
        Infosecurity = 6,

        ///<summary>
        /// 影像監控
        /// </summary>
        [Description("影像監控")]
        Monitor = 7,

        ///<summary>
        /// 智慧門管
        /// </summary>
        [Description("智慧門管")]
        Access = 8,

        ///<summary>
        /// GPS車隊管理
        /// </summary>
        [Description("GPS車隊管理")]
        GPS = 9,

        ///<summary>
        /// 保全服務
        /// </summary>
        [Description("保全服務")]
        Security = 10,

        ///<summary>
        /// 智慧綠建築
        /// </summary>
        [Description("智慧綠建築")]
        SmartGreen = 11,

        ///<summary>
        /// 智慧防災
        /// </summary>
        [Description("智慧防災")]
        SmartDisasterPrevention = 12,


        ///<summary>
        /// 智慧影像保全
        /// </summary>
        [Description("智慧影像保全")]
        AISecurity = 13,

        ///<summary>
        /// WorkLink
        /// </summary>
        [Description("WorkLink")]
        WorkLink = 14,

        ///<summary>
        /// 智能旅館
        /// </summary>
        [Description("智能旅館")]
        AIHotel = 15
    }

    /// <summary>
    /// 財務訊息類別
    /// </summary>
    public enum FinancialType
    {
        /// <summary>
        /// 財務報告
        /// </summary>
        [Description("財務報告")]
        FinancialStatements = 1,

        /// <summary>
        /// 營業收入       
        /// </summary>
        [Description("營業收入")]
        IncomeStatement = 2,

        /// <summary>
        /// 年報       
        /// </summary>
        [Description("年報")]
        AnnualStatements = 3
    }

    /// <summary>
    /// 股東專欄類別
    /// </summary>
    public enum ShareholderType
    {
        /// <summary>
        /// 股東會
        /// </summary>
        [Description("股東會")]
        ShareholderMeeting = 4,

        /// <summary>
        /// 法說會       
        /// </summary>
        [Description("法說會")]
        InvestorConference = 5
    }

    /// <summary>
    /// Banner標題文字顏色
    /// </summary>
    public enum TextColor
    {
        /// <summary>
        /// 白色       
        /// </summary>
        [Description("白色")]
        White = 1,
        /// <summary>
        /// 黑色
        /// </summary>
        [Description("黑色")]
        Black = 2
    }

    /// <summary>
    /// 社會責任財務類別
    /// </summary>
    public enum CorporateSocialType
    {
        /// <summary>
        /// 關懷社會福利基金會       
        /// </summary>
        [Description("關懷社會福利基金會")]
        Welfare = 6,

        /// <summary>
        /// 文化藝術基金會       
        /// </summary>
        [Description("文化藝術基金會")]
        CultureAndArts = 7,
    }

    /// <summary>
    /// 公司治理類別
    /// </summary>
    public enum GovernanceType
    {
        /// <summary>
        /// 運作情形       
        /// </summary>
        [Description("運作情形")]
        Training = 8,

        /// <summary>
        /// 溝通情形       
        /// </summary>
        [Description("溝通情形")]
        InternalAudit = 9,

        /// <summary>
        /// 公司內規       
        /// </summary>
        [Description("公司內規")]
        Rules = 10,
        /// <summary>
        /// 運作情形2       
        /// </summary>
        [Description("運作情形2")]
        Training2 = 11,
        /// <summary>
        /// 運作情形3       
        /// </summary>
        [Description("運作情形3")]
        Training3 = 12,
        /// <summary>
        /// 運作情形4       
        /// </summary>
        [Description("運作情形4")]
        Training4 = 13
    }

    /// <summary>
    /// 新聞中心/活動類別
    /// </summary>
    public enum NewsType
    {
        /// <summary>
        /// 一般新聞
        /// </summary>
        [Description("一般新聞")]
        News = 1,

        /// <summary>
        /// 安心教室       
        /// </summary>
        [Description("安心教室")]
        ClassRoom = 2,

        /// <summary>
        /// 關懷社會福利基金會       
        /// </summary>
        [Description("關懷社會福利基金會")]
        Welfare = 3,

        /// <summary>
        /// 文化藝術基金會       
        /// </summary>
        [Description("文化藝術基金會")]
        CultureAndArts = 4,

        /// <summary>
        /// 股東會重大消息       
        /// </summary>
        [Description("股東會-重大消息")]
        ShareHolderNews = 5
    }

    /// <summary>
    /// 活動內容區塊分類
    /// </summary>
    public enum EventInformationType
    {
        /// <summary>
        /// 模板A 文字
        /// </summary>
        [Description("模板A：文字")]
        A = 1,

        /// <summary>
        /// 模板B：首頁視覺       
        /// </summary>
        [Description("模板B：首頁視覺")]
        B = 2,

        /// <summary>
        /// 模板C：左圖右字(圖影皆可放)   
        /// </summary>
        [Description("模板C：圖與字可以互換位置(圖影皆可放)")]
        C = 3,

        ///// <summary>
        ///// 模板C：右圖左字(圖影皆可放)    
        ///// </summary>
        //[Description("模板C：右圖左字(圖影皆可放)")]
        //CRight = 4,

        /// <summary>
        /// 模板D：滿版底圖一文字區塊 (文字有底塊)    
        /// </summary>
        [Description("模板D：滿版底圖一文字區塊(文字有底塊)")]
        D = 4,

        /// <summary>
        /// 模板E：滿版底圖一文字區塊(文字無底塊)->可拿掉字只有圖       
        /// </summary>
        [Description("模板E：滿版底圖一文字區塊(文字無底塊)->可拿掉字只有圖")]
        E = 5,
        /// <summary>
        /// 模板F：卡片       
        /// </summary>
        [Description("模板F：卡片")]
        F = 6,
        /// <summary>
        /// 模板G：大icon
        /// </summary>
        [Description("模板G：大icon")]
        G = 7,
        /// <summary>
        ///模板H：影片
        /// </summary>
        [Description("模板H：影片")]
        H = 8,
    }
    /// <summary>
    /// 按鈕位置
    /// </summary>
    public enum CssPosition
    {
        /// <summary>
        /// 置左       
        /// </summary>
        [Description("置左")]
         Left = 1,

        /// <summary>
        /// 置右       
        /// </summary>
        [Description("置右")]
        Right = 2
    }

    /// <summary>
    /// 顯示比數
    /// </summary>
    public enum ShowItemNum
    {
        /// <summary>
        /// 2個       
        /// </summary>
        [Description("2個")]
        Two = 2,

        /// <summary>
        /// 3個       
        /// </summary>
        [Description("3個")]
        Three = 3
    }
    /// <summary>
    /// 圖片/影片/背景色
    /// </summary>
    public enum ObjectType
    {
        /// <summary>
        /// 圖片
        /// </summary>
        [Description("圖片")]
        Image = 1,
        /// <summary>
        /// Youtube影片連結
        /// </summary>
        [Description("Youtube影片連結")]
        Video = 2,
        /// <summary>
        /// 背景顏色
        /// </summary>
        [Description("背景顏色")]
        Color = 3
    }
    /// <summary>
    /// 字型
    /// </summary>
    public enum FontFamilyType
    {
        /// <summary>
        /// 微軟正黑體
        /// </summary>
        [Description("微軟正黑體")]
        MicrosoftJhengHei = 1,
        /// <summary>
        /// 新細明體
        /// </summary>
        [Description("新細明體")]
        MingLiU = 2,
        /// <summary>
        /// 標楷體
        /// </summary>
        [Description("標楷體")]
        DFKaiSB = 3

    }
}