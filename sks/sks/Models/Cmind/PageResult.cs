﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Models.Cmind
{
    public class PageResult<T> : Page
    {
        public List<T> Data { get; set; }
    }
}