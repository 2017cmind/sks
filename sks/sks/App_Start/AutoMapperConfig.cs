﻿using AutoMapper;
using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 前台
                cfg.CreateMap<ViewModels.Contact.ContactView, Contact> ();
                cfg.CreateMap<Contact, ViewModels.Contact.ContactView>();

                cfg.CreateMap<Product, ViewModels.Product.ProductView>();

                cfg.CreateMap<News, ViewModels.News.NewsView>();
                cfg.CreateMap<Event, ViewModels.Event.EventView>();
                cfg.CreateMap<EventContactChangeFieldName, ViewModels.Event.EventView>();
                cfg.CreateMap<EventInformationItem, ViewModels.Event.EventView>();
                cfg.CreateMap<EventInformation, ViewModels.Event.EventView>();
                cfg.CreateMap<EventContactChangeFieldName, ViewModels.Event.EventContactChangeFieldNameView>();
                cfg.CreateMap<ViewModels.Event.EventView, EventContact>();
                cfg.CreateMap<EventContact, ViewModels.Event.EventView>();
                #endregion
            });
        }
    }
}