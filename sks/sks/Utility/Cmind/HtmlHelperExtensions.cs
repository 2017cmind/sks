﻿using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.Utility.Cmind
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString AntiXssRaw(string rawHtml) =>
            MvcHtmlString.Create(Sanitizer.GetSafeHtmlFragment(rawHtml));

    }
}