﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class BannerRepository
    {

        public List<Banner> Query(bool? status,string title,int type)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                var query = db.Banner.AsQueryable();
                if (status.HasValue)
                {
                    query = query.Where(p => p.Status == status);
                }
                if (!string.IsNullOrEmpty(title))
                {
                    query = query.Where(p => p.Title.Contains(title));
                }
                if (type != 0)
                {
                    query = query.Where(p => p.Type == type);
                }
                return query.ToList();
            }
        }

    }
}