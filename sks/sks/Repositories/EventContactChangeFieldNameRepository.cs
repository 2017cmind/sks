﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class EventContactChangeFieldNameRepository
    {
        private SksDBEntities db = new SksDBEntities();

        public EventContactChangeFieldName FindBy(int id)
        {
            var query = db.EventContactChangeFieldNames.Find(id);
            return query;
        }
    }
}