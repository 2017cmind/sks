﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class NewsRepository
    {
        public List<News> List(int type, int year)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                var query = db.News.Where(p => p.Status == true && p.OnlineTime <= DateTime.Now);
                if (type != 0)
                {
                    query = query.Where(p => p.Type == type);
                }
                if (year != 0)
                {
                    query = query.Where(p => p.OnlineTime.Year == year);
                }
                return query.ToList();
            }     
        }
        public News GetById(int id)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                return db.News.Find(id);
            }
        }
    }
}