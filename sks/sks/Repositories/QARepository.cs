﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class QARepository
    {
        public QA GetById(int id)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                return db.QA.Find(id);
            }
        }
    }
}