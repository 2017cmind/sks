﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class ContactRepository
    {
        public int Insert(Contact newData)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                DateTime now = DateTime.Now;
                newData.CreateTime = now;
                db.Contact.Add(newData);
                db.SaveChanges();
                return newData.ID;
            }    
        }
        public Contact GetById(int id)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                return db.Contact.Find(id);
            }
        }
    }
}