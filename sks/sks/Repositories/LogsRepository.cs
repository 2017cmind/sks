﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class LogsRepository
    {
        private SksDBEntities db = new SksDBEntities();

        public IQueryable<Logs> Query(DateTime? date)
        {
            var query = GetAll();
            if (date.HasValue)
            {
                query = query.Where(p => p.CreateTime.Year == date.Value.Year && p.CreateTime.Month == date.Value.Month && p.CreateTime.Day == date.Value.Day);
            }
            return query;
        }

        public IQueryable<Logs> GetAll()
        {
            var query = db.Logs;
            return query;
        }

        public void Insert(Logs newData)
        {
            DateTime now = DateTime.Now;
            newData.CreateTime = now;
            db.Logs.Add(newData);
            db.SaveChanges();
        }

        public void Delete(DateTime date)
        {
            var delete = db.Logs.Where(p => p.CreateTime.Year == date.Year && p.CreateTime.Month == date.Month && p.CreateTime.Day == date.Day).ToList();
            db.Logs.RemoveRange(delete);
            db.SaveChanges();
        }
    }
}