﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class ProductRepository
    {

        public List<Product> Query(bool? status, string name,int type)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                var query = db.Product.AsQueryable();
                if (status.HasValue)
                {
                    query = query.Where(p => p.Status == status);
                }
                if (!string.IsNullOrEmpty(name))
                {
                    query = query.Where(p => p.Name.Contains(name));
                }
                if (type != 0)
                {
                    query = query.Where(p => p.Type == type);
                }
                return query.ToList();
            }
        }

        public IQueryable<Product> List(int type)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                var query = db.Product.Where(p => p.Status == true);
                if (type != 0)
                {
                    query = query.Where(p => p.Type == type);
                }
                return query;
            }
        }

        public string DeleteImage(int id)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                Product image = db.Product.Find(id);
                var delete = image.MainImage;
                image.MainImage = string.Empty;
                db.SaveChanges();
                return delete;
            }
        }
    }
}