﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using NLog;
using sks.Models;

namespace sks.Repositories
{
    public class EventContactRepository
    {
        private SksDBEntities db = new SksDBEntities();
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public int Insert(EventContact newData)
        {
            try
            {
                DateTime now = DateTime.Now;
                newData.CreateTime = now;
                db.EventContact.Add(newData);
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var entityError = ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.ErrorMessage);
                var getFullMessage = string.Join("; ", entityError);
                var exceptionMessage = string.Concat(ex.Message, "errors are: ", getFullMessage);
                //NLog
                logger.Info(string.Format("Error: {0}, Detail Error",
                    exceptionMessage, ex));
            }
            return newData.ID;
        }
        public EventContact FindBy(int id)
        {
            var query = db.EventContact.Find(id);
            return query;
        }

        public IQueryable<EventContact> GetAll()
        {
            var query = db.EventContact;
            return query;
        }

        public IQueryable<EventContact> Query(int eventId, string name, string email, string phone, DateTime? startDate, DateTime? endDate)
        {
            if (!startDate.HasValue)
            {
                startDate = DateTime.MinValue;
            }
            if (!endDate.HasValue)
            {
                endDate = DateTime.Now;
            }

            var query = GetAll().Where(p => p.CreateTime >= startDate && p.CreateTime <= endDate.Value);

            if (eventId != 0)
            {
                query = query.Where(p => p.EventId == eventId);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.Contains(name));
            }
            if (!string.IsNullOrEmpty(email))
            {
                query = query.Where(p => p.Email.Contains(email));
            }
            if (!string.IsNullOrEmpty(phone))
            {
                query = query.Where(p => p.Phone.Contains(phone));
            }

            return query;
        }
        public void Update(EventContact newData)
        {
            DateTime now = DateTime.Now;
            EventContact oldData = db.EventContact.Find(newData.ID);
            oldData.Name = newData.Name;
            oldData.ContactRemark = newData.ContactRemark;
            oldData.EventContactCategory = newData.EventContactCategory;
            oldData.Phone = newData.Phone;
            oldData.Address = newData.Address;
            oldData.Remark = newData.Remark;
            oldData.Email = newData.Email;
            oldData.UpdateTime = now;
            db.SaveChanges();
        }
    }
}