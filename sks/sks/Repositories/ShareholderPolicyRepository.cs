﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class ShareholderPolicyRepository 
    {
        public ShareholderPolicy GetById(int id)
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                return db.ShareholderPolicy.Find(id);
            }
        }
    }
}