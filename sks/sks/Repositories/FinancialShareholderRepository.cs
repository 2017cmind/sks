﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class FinancialShareholderRepository 
    {    
        public List<FinancialShareholder> Query(bool? status, string title, int type, int year, string fileName = "")
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                var query = db.FinancialShareholder.AsQueryable();
                if (!string.IsNullOrEmpty(title))
                {
                    query = query.Where(p => p.Title.Contains(title));
                }
                if (status.HasValue)
                {
                    query = query.Where(p => p.Status == status);
                }
                if (year != 0)
                {
                    query = query.Where(p => p.FiscalYear == year);
                }
                if (type != 0)
                {
                    query = query.Where(p => p.Type == type);
                }

                if (!string.IsNullOrEmpty(fileName))
                {
                    query = query.Where(p => p.FileName == fileName);
                }
                return query.ToList();
            }
        }
    }
}