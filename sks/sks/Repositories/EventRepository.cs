﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using NLog;
using sks.Models;

namespace sks.Repositories
{
    public class EventRepository
    {
        private SksDBEntities db = new SksDBEntities();
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public int Insert(Event newData) {
            try
            {
                DateTime now = DateTime.Now;
                newData.CreateTime = now;
                newData.UpdateTime = now;
                db.Events.Add(newData);
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var entityError = ex.EntityValidationErrors.SelectMany(x => x.ValidationErrors).Select(x => x.ErrorMessage);
                var getFullMessage = string.Join("; ", entityError);
                var exceptionMessage = string.Concat(ex.Message, "errors are: ", getFullMessage);
                //NLog
                logger.Info(string.Format("Error: {0}, Detail Error",
                    exceptionMessage, ex));                
            }


            return newData.ID;
        }
        public void Update(Event newData)
        {
            DateTime now = DateTime.Now;
            Event oldData = db.Events.Find(newData.ID);
            oldData.LinkName = newData.LinkName;
            oldData.MetaTitle = newData.MetaTitle;
            oldData.FontFamily = newData.FontFamily;
            oldData.MetaDesc = newData.MetaDesc;
            oldData.StartDate = newData.StartDate;
            oldData.EndDate = newData.EndDate;
            oldData.ContactTitle = newData.ContactTitle;
            oldData.ContactPhoneBgColor = newData.ContactPhoneBgColor;
            oldData.ContactPhoneText = newData.ContactPhoneText;
            oldData.ContactEmail = newData.ContactEmail;
            oldData.ContactDesc = newData.ContactDesc;
            oldData.ContactBg = newData.ContactBg;
            oldData.ContactBtnText = newData.ContactBtnText;
            oldData.ContactBtnBgColor = newData.ContactBtnBgColor;
            oldData.isContactStatus = newData.isContactStatus;
            oldData.IsVerification = newData.IsVerification;
            oldData.UpdateTime = now;
            var removes = db.EventInformations.Where(p => p.EventId == newData.ID);
            foreach (var item in removes) {
                var removeitems = db.EventInformationItems.Where(p => p.EventInformationId == item.ID);
                db.EventInformationItems.RemoveRange(removeitems);
            }
            
           var removeEventCategorys = db.EventContactChangeFieldNames.Where(p => p.EventId == newData.ID);
            db.EventContactChangeFieldNames.RemoveRange(removeEventCategorys);
            db.EventInformations.RemoveRange(removes);
            oldData.EventContactChangeFieldNames = newData.EventContactChangeFieldNames;
            oldData.EventInformations = newData.EventInformations;

            db.SaveChanges();
        }

        public IQueryable<Event> GetAll()
        {
            var query = db.Events;
            return query;
        }

        public void Delete(int id)
        {
            Event delete = db.Events.Find(id);
            var removes = db.EventInformations.Where(p => p.EventId == id);
            foreach (var item in removes)
            {
                var removeitems = db.EventInformationItems.Where(p => p.EventInformationId == item.ID);
                db.EventInformationItems.RemoveRange(removeitems);
            }
            var removeEventCategorys = db.EventContactChangeFieldNames.Where(p => p.EventId == id);
            db.EventContactChangeFieldNames.RemoveRange(removeEventCategorys);
            db.EventInformations.RemoveRange(removes);
            db.Events.Remove(delete);
            db.SaveChanges();
        }

        public void DeleteImage(int id, int type)
        {
            EventInformationItem delete = db.EventInformationItems.Find(id);
            switch (type)
            {
                case 1 :
                    delete.Background = string.Empty;
                    break;
                case 2:
                    delete.MainPic = string.Empty;
                    break;
                default:
                    delete.Icon = string.Empty;
                    break;
            }
            db.SaveChanges();
        }

        public Event FindBy(int id)
        {
            var query = db.Events.Find(id);
            return query;
        }

        public EventInformationItem InformationitemFindBy(int id)
        {
            var query = db.EventInformationItems.Find(id);
            return query;
        }
        public EventInformation InformationFindBy(int id)
        {
            var query = db.EventInformations.Find(id);
            return query;
        }

        public Event FindIdByName(string name)
        {
            var query = db.Events.Where(p => p.LinkName == name).FirstOrDefault();
            return query;
        }

        public IQueryable<Event> Query(string metatitle, DateTime ? startDate, DateTime ? endDate)
        {
            if (!startDate.HasValue)
            {
                startDate = DateTime.MinValue.Date;
            }
            var query = GetAll().Where(p => p.StartDate >= startDate);

            if (endDate.HasValue)
            {
                query = query.Where(p => p.EndDate <= endDate);
            }
            if (!string.IsNullOrEmpty(metatitle))
            {
                query = query.Where(p => p.MetaTitle.Contains(metatitle));
            }

            return query;
        }
    }
}