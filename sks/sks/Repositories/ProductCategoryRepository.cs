﻿using sks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sks.Repositories
{
    public class ProductCategoryRepository 
    {
        public List<ProductCategory> List()
        {
            using (SksDBEntities db = new SksDBEntities())
            {
                var query = db.ProductCategory.Where(p => p.Status == true);
                return query.ToList();
            }
        }
    }
}