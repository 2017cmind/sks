﻿function checkEmail(email) {
    var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return re.test(String(email).toLowerCase());
}
function checkPhone(phone) {
    var re = /(02|03|037|04|049|05|06|07|08|082|0826|0836|089{2,4})\d{6,8}$/;
    return re.test(String(phone).toLowerCase());
}
function checkMobliePhone(phone) {
    var re = /09\d{8}$/;
    return re.test(String(phone));
}

function checkName(text) {
    var regex = /^[\u4e00-\u9fa5]{2,15}$/;
    return regex.test(String(text).toLowerCase());
}
function checkMessage(text) {
    var regex = /^[\u4e00-\u9fa5，.,A-Za-z-]{15,}$/;
    return regex.test(String(text).toLowerCase());
}