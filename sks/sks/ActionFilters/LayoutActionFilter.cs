﻿using AutoMapper;
using sks.ViewModels.ProductCategory;
using sks.Models;
using sks.Repositories;
using sks.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sks.ActionFilters
{
    public class LayoutActionFilter : ActionFilterAttribute
    {
        private ProductCategoryRepository productCategoryRepository = new ProductCategoryRepository();
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Result is ViewResultBase)//Gets ViewResult and PartialViewResult
            {

                object viewModel = ((ViewResultBase)filterContext.Result).Model;

                if (viewModel != null && viewModel is HeaderView)
                {
                    HeaderView model = viewModel as HeaderView;
                    var categories = productCategoryRepository.List();
                    model.ProductMenus = Mapper.Map<List<ProductCategoryView>>(categories);
                }
            }

            base.OnActionExecuted(filterContext);
        }
    }
}